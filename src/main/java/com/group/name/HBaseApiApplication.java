package com.group.name;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class })
public class HBaseApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HBaseApiApplication.class, args);
	}

}
