package com.group.name.controller;

import com.group.name.model.bl.GetRequestBL;
import com.group.name.model.bl.MultiGetRequestBL;
import com.group.name.model.bl.ScanRequestBL;
import com.group.name.model.exceptions.HeaderValidationFailedException;
import com.group.name.model.exceptions.handlers.OperationFailedExceptionHandler;
import com.group.name.model.requests.GetRequestModelData;
import com.group.name.model.requests.MultiGetRequestModelData;
import com.group.name.model.requests.ScanRequestModelData;
import com.group.name.model.response.GetResponseModelData;
import com.group.name.model.validation.UserValidation;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api") // http:localhost:8080/api
@Validated // Enable validation for both request parameters
public class UIController {

    private GetRequestBL getRequestBL;
    private MultiGetRequestBL multiGetRequestBL;
    private ScanRequestBL scanRequestBL;

    public UIController() {
        this.getRequestBL = new GetRequestBL();
        this.multiGetRequestBL = new MultiGetRequestBL(this.getRequestBL);
        this.scanRequestBL = new ScanRequestBL();
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
    @RequestMapping("/isAlive")
    public Boolean isAlive() {
        return Boolean.TRUE;
    }

    @RequestMapping("/row")
    // get one row
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GetResponseModelData> getOneRow(@RequestParam(value = "tableName") @NotNull @NotEmpty String tableName,
                                                          @RequestParam(value = "rowKey") @NotNull @NotEmpty String rowKey,
                                                          @RequestParam(value = "username") @NotNull @NotEmpty @UserValidation String username) {
        try {
            GetResponseModelData getResponseModelData = getRequestBL.get(new GetRequestModelData(tableName, rowKey, username), true);
            return new ResponseEntity<>(getResponseModelData, HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
            throw new OperationFailedExceptionHandler();
        }
    }

    @RequestMapping("/rows")
    // get multiple rows
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getMultiRows(@Valid @RequestBody MultiGetRequestModelData multiGetRequestModelData,
                                       @RequestHeader HttpHeaders header) {
        // header validation.
        List<String> methodOverrideHeaderValue = header.get("X-HTTP-Method-Override");
        if (methodOverrideHeaderValue != null && methodOverrideHeaderValue.contains("GET")) {
            return this.multiGetRequestBL.multiGet(multiGetRequestModelData);
        } else {
            throw new HeaderValidationFailedException("You have to include X-HTTP-Method-Override=GET header");
        }
    }

    @RequestMapping("/scan")
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity scan(@RequestBody @Valid ScanRequestModelData scanRequestModelData) {
        return scanRequestBL.scan(scanRequestModelData);
    }


}
