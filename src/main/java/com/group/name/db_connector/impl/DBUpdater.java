package com.group.name.db_connector.impl;

import com.group.name.model.utils.TimeUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * Update the data in the DB
 */
public class DBUpdater {

    private static DBUpdater dbUpdater = new DBUpdater();
    private static PreparedStatement preparedStatement;

    private DBUpdater() {
        try {
            Connection connection = HikariCPConnection.getConnection();
            preparedStatement = connection.prepareStatement(String.format(
                    "UPDATE %s " +
                            "SET requestsPerDay=requestsPerDay+?,recordsPerDay=recordsPerDay+?," +
                            "totalRequests=totalRequests+?,totalRecords=totalRecords+?,lastGet=?" +
                            " WHERE username=?", DBDefinitions.TABLE_NAME));

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Failed to initialize a connection to the DB");
        }
    }

    public static DBUpdater getInstance() {
        return dbUpdater;
    }

    //    UPDATE table_name
    //    SET column1 = value1, column2 = value2, ...
    //    WHERE condition;
    public synchronized int updateRequestNum(int newRequestNum, String username) {
        int resultSet = 0; // no changes
        Timestamp lastGetDate = TimeUtils.getCurrentDate();
        try {
            preparedStatement.setInt(1, 1); // requestsPerDay
            preparedStatement.setInt(2, newRequestNum); // recordsPerDay
            preparedStatement.setInt(3, 1); // totalRequests
            preparedStatement.setInt(4, newRequestNum); // totalRecords
            preparedStatement.setTimestamp(5, lastGetDate); // lastGet
            preparedStatement.setString(6, username); // username
            System.out.println("preparedStatement= " + preparedStatement);
            resultSet = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }
}
