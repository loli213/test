package com.group.name.db_connector.impl;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

// Reason for choosing HikariCP: https://www.wix.engineering/post/how-does-hikaricp-compare-to-other-connection-pools
public class HikariCPConnection {

    private static HikariDataSource ds = setupDataSource();

    private static HikariDataSource setupDataSource() {
        //HikariConfig config = new HikariConfig("dc_connection.properties");
        HikariConfig config = new HikariConfig(); //jdbc:mysql://localhost:3306/classicmodels
        //config.setConnectionInitSql("set time zone 'UTC'");
        config.setJdbcUrl(DBDefinitions.JDBC_URL);
        config.setDriverClassName("com.mysql.cj.jdbc.Driver");
        config.setUsername("root");
        config.setPassword("ofir1997");
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        return new HikariDataSource(config);
    }



    static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }


}
