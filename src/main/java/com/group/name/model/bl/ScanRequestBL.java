package com.group.name.model.bl;

import com.group.name.model.exceptions.OperationFailedException;
import com.group.name.model.requests.ScanRequestModelData;
import com.group.name.model.response.ScanResponseModelData;
import com.group.name.model.utils.SparkConnectionSetup;
import com.group.name.model.utils.TimeUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.CompareOperator;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.ColumnPrefixFilter;
import org.apache.hadoop.hbase.filter.ValueFilter;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

/**
 * Business logic of handling Scan request from the client.
 * Contains the limit & Updating the DB
 */
public class ScanRequestBL {
    // todo -log4j?
    private static final String BASE_FOLDER_PATH = "C:\\Users\\Ofir\\Downloads\\ScanResults"; // todo - change it.

    // TODO - username, DB.. Validation that the user can do this scan (DB & normal validation).
    public ResponseEntity scan(ScanRequestModelData scanRequestModelData) {
        String username = scanRequestModelData.getUsername();
        String tableName = scanRequestModelData.getTableName();
        String urlToNotify = scanRequestModelData.getUrlToNotify(); // todo - http post to this url.
        String emailToNotify = scanRequestModelData.getEmailToNotify(); // todo - implement emailToNotify
        List<String> selectedColumns = scanRequestModelData.getSelectedColumns();
        List<String> selectedPrefixColumns = scanRequestModelData.getSelectedPrefixColumns();
        List<String> selectedValues = scanRequestModelData.getSelectedValues();
        String startTimestampStr = scanRequestModelData.getStartTimestamp();
        String endTimestampStr = scanRequestModelData.getEndTimestamp();
        String startRow = scanRequestModelData.getStartRow();
        String endRow = scanRequestModelData.getEndRow();

        Scan scan = new Scan();
        scan.setCaching(500);
        scan.setCacheBlocks(false);

        try {

            // set time range.
            if (startTimestampStr != null && endTimestampStr != null) {
                long startTimestamp = TimeUtils.fromDateToUnixTimestamp(startTimestampStr);
                long endTimestamp = TimeUtils.fromDateToUnixTimestamp(endTimestampStr);
                scan.setTimeRange(startTimestamp, endTimestamp);
            }

            if (startRow != null && endRow != null) {
                scan.withStartRow(startRow.getBytes());
                scan.withStopRow(endRow.getBytes());
            }

            if (selectedColumns != null) {
                selectedColumns.forEach(currentColumn -> {
                    final String[] columnFamilyToColumn = currentColumn.split(":");
                    scan.addColumn(columnFamilyToColumn[0].getBytes(), columnFamilyToColumn[1].getBytes()); // todo - verify.. handling exception.
                });
            }

            if (selectedPrefixColumns != null) {
                selectedPrefixColumns.forEach(currentPrefixColumn -> {
                    scan.setFilter(new ColumnPrefixFilter(currentPrefixColumn.getBytes()));
                });
            }

            if (selectedValues != null) {
                selectedValues.forEach(currentValue -> {
                    scan.setFilter(new ValueFilter(CompareOperator.EQUAL,
                            new BinaryComparator(currentValue.getBytes())));
                });
            }
            String resultPath = BASE_FOLDER_PATH + "\\" + System.currentTimeMillis();

            // todo - replace thread with https://www.baeldung.com/spring-mvc-handlerinterceptor
            new Thread(() -> {
                try {
                    startScan(scan, tableName, resultPath);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }).start();

            // todo - notify email & send post request.
            String msg = String.format("Saved Scan result to: %s", resultPath);
            System.out.println(msg);
            ScanResponseModelData response = new ScanResponseModelData(resultPath, emailToNotify, urlToNotify, msg);
            return new ResponseEntity<>(response, HttpStatus.OK);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
            throw new OperationFailedException(e.getMessage());
        }


    }

    // todo - Pass a callback to this method (?)
    private void startScan(Scan scan, String tableName, String pathToSave) throws IOException {
        SparkConnectionSetup sparkConnectionSetup = new SparkConnectionSetup();
        JavaSparkContext javaSparkContext = sparkConnectionSetup.setup();
        Configuration conf = HBaseConfiguration.create();
        conf.set(TableInputFormat.INPUT_TABLE, tableName);


        conf.set(TableInputFormat.SCAN, TableMapReduceUtil.convertScanToString(scan));

        // Get RDD
        JavaPairRDD<ImmutableBytesWritable, Result> sourceRDD = javaSparkContext.newAPIHadoopRDD(conf,
                TableInputFormat.class, ImmutableBytesWritable.class, Result.class).cache(); // todo - check if cache() improves the running time.
        System.out.println("count = " + sourceRDD.count());

        sourceRDD.saveAsNewAPIHadoopFile(pathToSave, ImmutableBytesWritable.class, Result.class,
                TextOutputFormat.class, conf);
        javaSparkContext.close();
    }


}
