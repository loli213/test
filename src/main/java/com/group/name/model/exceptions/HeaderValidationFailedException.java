package com.group.name.model.exceptions;

public class HeaderValidationFailedException extends RuntimeException{

    public HeaderValidationFailedException(String msg) {
        super(msg);
    }

}
