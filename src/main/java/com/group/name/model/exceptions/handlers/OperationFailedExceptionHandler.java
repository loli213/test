package com.group.name.model.exceptions.handlers;

import com.group.name.model.exceptions.OperationFailedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class OperationFailedExceptionHandler extends RuntimeException {

    @ExceptionHandler(value = {OperationFailedException.class})
    public ResponseEntity<String> handle(OperationFailedExceptionHandler e) {
        String errorMsg = e.getMessage();
        System.out.println("Client request was failed, reason: " + errorMsg);
        return new ResponseEntity<>(errorMsg, HttpStatus.INTERNAL_SERVER_ERROR);
    }




}


