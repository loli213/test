package com.group.name.model.response;

import java.util.Map;

public class GetResponseModelData {

    private String rowKey;
    private Map<String, String> columnToValueMap;

    public GetResponseModelData(String rowKey, Map<String, String> columnToValueMap) {
        this.rowKey = rowKey;
        this.columnToValueMap = columnToValueMap;
    }

    public String getRowKey() {
        return rowKey;
    }

    public void setRowKey(String rowKey) {
        this.rowKey = rowKey;
    }

    public Map<String, String> getColumnToValueMap() {
        return columnToValueMap;
    }

    public void setColumnToValueMap(Map<String, String> columnToValueMap) {
        this.columnToValueMap = columnToValueMap;
    }
}
