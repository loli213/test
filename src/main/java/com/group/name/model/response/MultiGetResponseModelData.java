package com.group.name.model.response;

import java.util.List;

public class MultiGetResponseModelData {

    private List<GetResponseModelData> getResponseModelDataList;

    public MultiGetResponseModelData(List<GetResponseModelData> getResponseModelDataList) {
        this.getResponseModelDataList = getResponseModelDataList;
    }


    public List<GetResponseModelData> getGetResponseModelDataList() {
        return getResponseModelDataList;
    }

    public void setGetResponseModelDataList(List<GetResponseModelData> getResponseModelDataList) {
        this.getResponseModelDataList = getResponseModelDataList;
    }
}
