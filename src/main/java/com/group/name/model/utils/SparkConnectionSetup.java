package com.group.name.model.utils;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * Initialize a connection to the master.
 */
public class SparkConnectionSetup {

    public JavaSparkContext setup() {
        SparkConf sparkConf = new SparkConf().setMaster("local[4]").setAppName("HBaseScanner-Spark");
        return new JavaSparkContext(sparkConf);
    }
}
