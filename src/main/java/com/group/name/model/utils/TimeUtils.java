package com.group.name.model.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;

public class TimeUtils {

    private static final String DATE_FORMAT = "yyyy/MM/dd-HH:mm:ss";

    public static long fromDateToUnixTimestamp(String dateStr) throws ParseException {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        Date date = simpleDateFormat.parse(dateStr);
        return date.getTime();
    }

    public static boolean isRecently(Timestamp lastGetTimestamp, long hours) {
        LocalDateTime prev = lastGetTimestamp.toLocalDateTime();
        LocalDateTime now = LocalDateTime.now();
        Duration dur = Duration.between(prev, now);
        long passedHours = dur.toHours();
        return passedHours < hours;
    }

    public static Timestamp getCurrentDate() {
        return Timestamp.valueOf(LocalDateTime.now());
    }
}
