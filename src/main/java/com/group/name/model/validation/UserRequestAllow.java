package com.group.name.model.validation;

import com.group.name.db_connector.impl.DBPuller;
import com.group.name.model.utils.TimeUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

@Component
public class UserRequestAllow {

    // DBPuller is initialized by Dependency Injection.
    private final DBPuller dbPuller;

    private final static long RECENTLY_HOURS = 2; // todo - define it in a config.prop file.

    public UserRequestAllow(DBPuller dbPuller) {
        this.dbPuller = dbPuller;
    }


    /*
    validation process:
        1) verify that the user exists in the DB.
        2) verify that the user doesn't pass the allowed request.
            * If Not - return true.
            * Otherwise:
                decide if the request can be made by the last date
    */
    public boolean isValid(String username, int requestedRecordsNum) {
        ResultSet resultSet = dbPuller.getRow(username);

        try {
            if (resultSet == null || !resultSet.next()) {
                System.out.println("No such username: " + username);
                return false;
            }

            /**
             *     requestsPerDay INT UNSIGNED NOT NULL,
             *     recordsPerDay INT UNSIGNED NOT NULL,
             *     totalRequests BIGINT UNSIGNED NOT NULL,
             *     totalRecords BIGINT UNSIGNED NOT NULL,
             * 	lastGet TIMESTAMP NOT NULL,
             * 	allowedRecordsPerDay INT UNSIGNED NOT NULL,
             *     allowedRequestsPerDay INT UNSIGNED NOT NULL,
             *     totalAllowedRecords BIGINT UNSIGNED NOT NULL
             *     totalAllowedRequests BIGINT UNSIGNED NOT NULL
             */
            // get the values.
            int requestPerDay = resultSet.getInt("requestsPerDay");
            int recordsPerDay = resultSet.getInt("recordsPerDay");
            BigDecimal allowedRequestsPerDay = resultSet.getBigDecimal("allowedRequestsPerDay");
            BigDecimal allowedRecordsPerDay = resultSet.getBigDecimal("allowedRecordsPerDay");
            int totalRequests = resultSet.getInt("totalRequests");
            int totalRecords = resultSet.getInt("totalRecords");
            BigDecimal totalAllowedRequests = resultSet.getBigDecimal("totalAllowedRequests");
            BigDecimal totalAllowedRecords = resultSet.getBigDecimal("totalAllowedRecords");
            Timestamp lastGetTimestamp = resultSet.getTimestamp("lastGet");

            // TODO - IMPORTANT: should be replaced with: if (isAllowed(totalRequests + 1, totalAllowedRequests)) and check recordsPerDay with + requestedRowsNum.
            if (isAllowed(totalRecords + requestedRecordsNum, totalAllowedRecords) && isAllowed(totalRequests + 1, totalAllowedRequests)) {

                // check if the user passes the total request per day by this request.
                if (isAllowed(recordsPerDay + requestedRecordsNum, allowedRecordsPerDay) && isAllowed(requestPerDay + 1, allowedRequestsPerDay)) {
                    return true;
                } else {
                    // the user passes the limit of the request/records per day, but we would like to allow him
                    // to get this row, if his lastGetRequest was *not* recently and he askes for only one row.
                    return requestedRecordsNum == 1 && !TimeUtils.isRecently(lastGetTimestamp, RECENTLY_HOURS);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param first  first number, as an integer.
     * @param second second number, BigDecimal format.
     * @return true if first less than the second.
     */
    private boolean isAllowed(int first, BigDecimal second) {
        BigDecimal firstBigDec = new BigDecimal(first);
        return firstBigDec.compareTo(second) <= 0;
    }
}
