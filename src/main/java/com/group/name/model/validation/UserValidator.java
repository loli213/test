package com.group.name.model.validation;

import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


public class UserValidator implements ConstraintValidator<UserValidation, String> {

    @Autowired
    private UserRequestAllow userRequestAllow;


    @Override
    public void initialize(UserValidation constraintAnnotation) {
    }


    @Override
    public boolean isValid(String username, ConstraintValidatorContext constraintValidatorContext) {
        return userRequestAllow.isValid(username, 1);
    }



}
